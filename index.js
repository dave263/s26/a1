
const HTTP = require('http');

HTTP.createServer((request, response) => {

    if(request.url === "/login"){
    response.writeHead(200, {"content-Type": "text/plain"})
    response.write("Welcome to the login page.")
    response.end()
    } else if (request.url === "/register") {
        response.writeHead(404, {"content-Type": "text/plain"})
    response.write("I'm sorry the page you are looking for cannot be found.")
    response.end()
    }
}).listen(3000)